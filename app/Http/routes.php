<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::match(['get', 'post'], '/', ['uses' => 'AppController@home', 'as' => 'home']);
// Route::get('modifierScore', ['uses' => 'AppController@test', 'as' => 'test']);
Route::post('signup', 'AppController@signup');
// Route::post('share', 'AppController@share');
// Route::post('bkQoflJF', 'AppController@startGame');
// Route::post('GOlTRrhu', 'AppController@saveGame');
// Route::post('log', 'AppController@setLog');
// Route::get('getGameData', 'AppController@getGameData');

// Route::get('auth/facebookLogin', 'Auth\AuthController@facebookLogin');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'authz'],
    'roles' => ['admin'],
    'permissions' => ['can_edit']
], function()
{
    Route::get('/', ['uses' => 'AdminController@dashboard', 'as' => 'admin.dashboard']);
});
