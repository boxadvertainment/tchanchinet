<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Participant;
use App\Game;

class AppController extends Controller
{
    public function home(Request $request)
    {
        // if ( ! $request->isMethod('post') && !\App::environment('local') ) {
        //     return redirect(config('services.facebook.app_page'));
        // }
        // // Fix blocked third-party cookies in Safari
        // if ( count($request->cookie()) === 0 ) {
        //     return '<script> top.location.href = "' . route('home') . '"; </script>';
        // }

        $request->session()->forget('participantId');
        $classement = Participant::select('facebook_id', 'name', 'score')->where('cheater', 0)->orderBy('score', 'desc')->take(4)->get();

        return view('home', [ 'classement' => $classement]);
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:participants',
            'phone' => 'regex:/^[0-9]{8}$/|unique:participants',
            'cin' => 'required|regex:/^[0-9]{8}$/|unique:participants'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom et prénom',
            'email' => 'E-mail',
            'phone' => 'Téléphone',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $participant = session('participant');
        if (!$participant) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $participant->name = $request->input('name');
        $participant->email = $request->input('email');
        $participant->phone = $request->input('phone');
        $participant->cin = $request->input('cin');
        $participant->ip = $request->ip();

        if ($participant->save()) {
            session(['participantId' => $participant->id]);
            return response()->json(['success' => true, 'gameData' => $this->getGameData() ]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function share()
    {
        if (! session('participantId') )
            return response()->json('none');

        $participant = Participant::find( session('participantId') );
        if ($participant->has_shared ) {
            return response()->json('none');
        }

        $participant->has_shared = true;
        $participant->score += 1000000 ;
        if ($participant->save()) {
            return response()->json(['message' => 'Félicitations ! vous avez récupérer 10 point', 'gameData' => $this->getGameData()]);
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function startGame()
    {
        session()->forget('log');
        session(['beganOn' => new \DateTime() ]);
        return response()->json([ 'success' => true, 't' => session('beganOn')->getTimestamp() ]);
    }

    public function setLog(Request $request)
    {
        $request->session()->push('log', $request->input('l') . ',' .$request->input('s') . ',' . $request->input('t'));
        return response()->json([ 'success' => true ]);
    }

    public function saveGame(Request $request)
    {
        if (!session('participantId'))
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $participant = Participant::find( session('participantId') );

        $beganOn = session('beganOn');
        if (!$beganOn) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $finishedOn = new \DateTime();
        $elapsedTime = $finishedOn->getTimestamp() - $beganOn->getTimestamp() - 36;

        $currentLevel = $participant->games()->count() + 1;
        $max = 1;
        if (new \DateTime() > new \DateTime('2015-07-13')) {
            $max = 3;
        } else if (new \DateTime() > new \DateTime('2015-07-08')) {
            $max = 2;
        }

        $validator = \Validator::make( $request->all(), [
            'level' => 'required|numeric|between:1,3|max:'.$currentLevel.'|max:'.$max,
            'score' => 'required|numeric',
            'time' => 'required|numeric'
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        \DB::table('games_history')->insert([
            'participant_id' => $participant->id,
            'level' => $request->input('level'),
            'score' => $request->input('score'),
            'score_a' => 0 ,
            'time' => $request->input('time'),
            'time_a' => $elapsedTime,
            'log' =>  serialize(session('log')),
            'referer' =>  \Request::header('referer'),
            'created_at' => new \DateTime()
        ]);

        $game = Game::firstOrNew(['participant_id' => $participant->id, 'level' => $request->input('level') ]);
        if ($game->score > $request->input('score')) {
            return response()->json(['gameData' => $this->getGameData()]);
        }

        $game->score = $request->input('score');
        $game->score_a = 0;
        $game->time = $request->input('time');
        $game->time_a = $elapsedTime;
        $game->level = $request->input('level');
        $game->log =  serialize(session('log'));
        $game->referer =  \Request::header('referer');

        if ( $game->save() ) {
            $participant->score = ($participant->has_shared) ? $participant->games()->sum('score') + 1000000 : $participant->games()->sum('score');
            if ($participant->save()) {
                return response()->json( ['gameData' => $this->getGameData(), 't' => session('beganOn')->getTimestamp(), 'f' => $finishedOn->getTimestamp() , 'et' => $elapsedTime ]);
            }
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }


    public function getGameData()
    {
        if (!session('participantId'))
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $participant = Participant::select('id', 'facebook_id', 'name', 'score', 'has_shared')->find(session('participantId'))->toArray();
        $participant['rank'] = Participant::where('score', '>', $participant['score'] )->where('cheater', 0)->count() + 1;
        $levels = Game::where('participant_id', $participant['id'])->orderBy('level')->lists('score')->toArray();
        $classement = Participant::select('facebook_id', 'name', 'score')->where('id', '<>', $participant['id'])->where('cheater', 0)->orderBy('score', 'desc')->take(4)->get()->toArray();

        return [
            'classement' => $classement,
            'userInfo' => $participant,
            'levels' => $levels
        ];
    }

    public function test() {
        Participant::chunk(200, function ($participants) {
            foreach ($participants as $participant) {
                $participant->score = ($participant->has_shared) ? $participant->games()->sum('score') + 1000000 : $participant->games()->sum('score');
                $participant->save();
            }
        });

        return response('adf');
    }

}
