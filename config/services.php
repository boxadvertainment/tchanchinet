<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

	'mandrill' => [
		'secret' => 'tXI5A0bhNM2o-N2VHdDMoQ',
	],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],

	/*
    |--------------------------------------------------------------------------
    | Facebook App Config
    |--------------------------------------------------------------------------
    */

	'facebook' => [
		'client_id'     => '1123877997628902',
		'client_secret' => '0637b61900da33a8e1a154006386ca3a',
		'redirect'      => env('APP_URL') ,
		'app_page'      => 'https://www.facebook.com/ooredootn/app_1123877997628902',
	],

];
