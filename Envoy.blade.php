@servers(['web' => 'root@smart-robox.com'])

@setup
    $directory = "/home/ooredoo/tchanchinet";
    $repository = "git@bitbucket.org:boxadvertainment/tchanchinet.git";
@endsetup

@macro('create')
    clone
    configure
@endmacro

@macro('deploy')
    pull
    configure
@endmacro

@macro('migrate')
    configure
    updateMigrations
@endmacro

@macro('rollback')
    configure
    rollbackMigrations
@endmacro

@macro('seed')
    configure
    seedDatabase
@endmacro

@task('pull')
    cd {{ $directory }};

    php artisan down;

    git pull origin master;
    composer install --prefer-dist --no-dev --no-interaction;

    php -r "copy('.env.production', '.env');";

    php artisan up;
    echo "Deployment finished successfully!";
@endtask

@task('clone')
    git clone -b master {{ $repository }} {{ $directory }};

    cd {{ $directory }};
    composer self-update;
    composer install --prefer-dist --no-dev --no-interaction;

    php -r "copy('.env.production', '.env');";

    echo "Project has been created";
@endtask

@task('updateMigrations')
    cd {{ $directory }};
    php artisan migrate --force;
@endtask

@task('rollbackMigrations')
    cd {{ $directory }};
    php artisan migrate:rollback --force;
@endtask

@task('seedDatabase')
    cd {{ $directory }};
    php artisan db:seed --force;
@endtask

@task('configure')
    cd {{ $directory }};

    php artisan config:cache;
    php artisan route:cache;

    chown -R www-data:www-data {{ $directory }};
    echo "Permissions have been set";
@endtask