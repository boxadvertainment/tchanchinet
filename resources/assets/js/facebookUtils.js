var facebookUtils = (function($) {
  var access_token, loginStatus, loginCallback, oldValue;

  function handleRedirectLogin() {
    var queryString = window.location.hash.substring(1);
    window.location.hash = '';
    var params = queryString.split('&');
    var accessToken;

    for (var i in params) {
      var pair = params[i].split("=");
      if (pair[0] == 'access_token')
        accessToken = pair[1];
    }

    if ( accessToken ) {
      config.targetButton.click();
    }
  }

  var config = {
    appId: '',
    scopes: [],
    requiredScopes : [],
    targetButton: null,
    onComplete: function() {
    }
  }

  function init(options) {
    $.extend(config, config, options);
    disableTargetButton();

    $.ajax({
      url: '//connect.facebook.net/fr_FR/sdk.js',
      dataType: "script",
      cache: true
    }).done(function() {
      FB.init({
        appId: config.appId,
        status: true,
        xfbml: true,
        version: 'v2.3'
      });

      setLoginStatus(function(){
        enableTargetButton();
        handleRedirectLogin();
        config.onComplete();
      });
    });
  }

  function setLoginStatus(callback) {
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        access_token = response.authResponse.accessToken;

        checkPermissions(function(granted) {
          loginStatus = (granted) ? 'connected' : 'missing_permissions' ;
          callback();
        });
      } else {
        loginStatus = response.status;
        callback();
      }
    });
  }

  function checkPermissions(callback) {
    FB.api('/me/permissions', function(response) {
      var currentPermissions = $.map(response.data, function(item) {
        return item.permission;
      });

      for (var i in config.requiredScopes) {
        var item = currentPermissions.indexOf(config.requiredScopes[i]);
        if ( item == -1 || response.data[item].status === 'declined' ) {
          return callback(false);
        }
      }
      callback(true);
    });
  }

  function handleMissingPermissions(grantedScopes) {
    for (var i in config.requiredScopes) {
      if ( grantedScopes.indexOf(config.requiredScopes[i]) == -1 ) {
        sweetAlert('Permissions requises', 'Pour vous offrir une expérience personnalisée, vous devez autoriser l\'application à accéder à ces informations: ', 'info');
        return false;
      }
    }
    return true;
  }

  function sendLoginRequest() {
    disableTargetButton();
    $.ajax({
      url: BASE_URL + "/auth/facebookLogin",
      data: {access_token: access_token}
    }).done(function(response) {
      enableTargetButton();
      loginCallback(response);
    });
  }

  function promptLogin() {
    if( navigator.userAgent.match('CriOS') )
      return window.location = 'https://m.facebook.com/dialog/oauth?client_id=' + config.appId + '&redirect_uri=' +
      CURRENT_URL +'&scope=' + config.scopes.join() + '&auth_type=rerequest&response_type=token';

    FB.login(function(response) {
      if (response.status == 'connected' &&
          response.authResponse &&
          response.authResponse.grantedScopes &&
          handleMissingPermissions( response.authResponse.grantedScopes.split(',') )
      ) {
        access_token = response.authResponse.accessToken;
        sendLoginRequest();
      }
    }, {
      scope: config.scopes.join(),
      auth_type: 'rerequest',
      return_scopes: true
    });
  }

  function login(callback) {
    loginCallback = callback;

    if ( loginStatus === 'connected' ) {
      sendLoginRequest();
    } else {
      promptLogin();
    }
  }

  function fetchUserInfo(callback) {
    FB.api('/me', function(response) {
      callback(response);
    });
  }

  function share(url, callback) {
    FB.ui({
      method: 'share',
      href: url
    }, callback);
  }

  function feed(params, callback) {
    $.extend(params, { method: 'feed' });
    FB.ui(params, callback);
  }

  function sendAppRequest(message, callback) {
    FB.ui({
      method: 'apprequests',
      message: message
    }, callback);
  }

  function addPageTab() {
    FB.ui({
      method: 'pagetab',
      redirect_uri: CURRENT_URL
    });
  }

  function resizeCanvas(params) {
    // This method is only enabled when Canvas Height is set to "Fluid" in the App Dashboard
    FB.Canvas.setSize(params);
  }

  function disableTargetButton() {
    oldValue = $(config.targetButton).html();
    $(config.targetButton).html('Loading...').prop('disabled', true).addClass('loading');
  }

  function enableTargetButton() {
    $(config.targetButton).html( oldValue ).prop('disabled', false).removeClass('loading');
  }

  return {
    init: init,
    login: login,
    fetchUserInfo: fetchUserInfo,
    share: share,
    feed: feed,
    sendAppRequest: sendAppRequest,
    resizeCanvas: resizeCanvas
  }
})(jQuery);