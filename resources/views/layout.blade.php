<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title', Config::get('app.name'))</title>
    <meta name="description" content="@yield('description')">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="{{ Config::get('services.facebook.client_id') }}"/>
    <meta property="og:image" content="{{ asset('img/fb-share.jpg') }}"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="@yield('meta.url', url('/'))"/>
    <meta property="og:site_name" content="{{ Config::get('app.name') }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="@yield('description')"/>

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontello.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}?v=3">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script>
        BASE_URL    = '{{ URL::to('/') }}';
        CURRENT_URL = '{{ URL::full() }}';
        USER_AUTH   = {{ Auth::check() ? 'true' : 'false' }};
        FB_APP_ID   = '{{ Config::get('services.facebook.client_id') }}';

        // Redirect App from Canvas Page to Page Tab
        if ( document.referrer && document.referrer.indexOf("apps.facebook.com") !== -1) {
            window.top.location.href = "{{ config('services.facebook.app_page') }}";
        }
    </script>
</head>
<body class="@yield('class')">
<?php exit('<img src="img/fin-du-jeu.jpg" width="810" height="730">'); ?>

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <main class="main" role="main">
        @yield('content')
    </main>

    @yield('body')

    <div id="preloader">
        <img src="img/frt.png" width="1" height="1" />
        <img src="img/frt2.png" width="1" height="1" />
        <img src="img/rep-lvl1.png" width="1" height="1" />
        <img src="img/rep-lvl2.png" width="1" height="1" />
        <img src="img/rep-lvl3.png" width="1" height="1" />
        <img src="img/bg-drags.png" width="1" height="1" />
        <img src="img/icn/1.png" width="1" height="1" />
        <img src="img/icn/2.png" width="1" height="1" />
        <img src="img/icn/3.png" width="1" height="1" />
        <img src="img/icn/4.png" width="1" height="1" />
        <img src="img/icn/5.png" width="1" height="1" />
        <img src="img/icn/6.png" width="1" height="1" />
        <img src="img/icn/7.png" width="1" height="1" />
        <img src="img/icn/8.png" width="1" height="1" />
        <img src="img/icn/9.png" width="1" height="1" />
        <img src="img/icn/10.png" width="1" height="1" />
        <img src="img/icn2/1.png" width="1" height="1" />
        <img src="img/icn2/2.png" width="1" height="1" />
        <img src="img/icn2/3.png" width="1" height="1" />
        <img src="img/icn2/4.png" width="1" height="1" />
        <img src="img/icn2/5.png" width="1" height="1" />
        <img src="img/icn2/6.png" width="1" height="1" />
        <img src="img/icn2/7.png" width="1" height="1" />
        <img src="img/icn2/8.png" width="1" height="1" />
        <img src="img/icn2/9.png" width="1" height="1" />
        <img src="img/icn2/10.png" width="1" height="1" />

        <img src="img/icn3/phrase1/mail.png" width="1" height="1">
        <img src="img/icn3/phrase1/li9a.png" width="1" height="1">
        <img src="img/icn3/phrase1/tjib.png" width="1" height="1">
        <img src="img/icn3/phrase1/7iit.png" width="1" height="1">
        <img src="img/icn3/phrase2/ghir-rbo3.png" width="1" height="1">
        <img src="img/icn3/phrase2/tawa.png" width="1" height="1">
        <img src="img/icn3/phrase2/lol.png" width="1" height="1">
        <img src="img/icn3/phrase2/ghir.png" width="1" height="1">
        <img src="img/icn3/phrase3/koura.png" width="1" height="1">
        <img src="img/icn3/phrase3/mala.png" width="1" height="1">
        <img src="img/icn3/phrase3/tag.png" width="1" height="1">
        <img src="img/icn3/phrase3/lyoum.png" width="1" height="1">
        <img src="img/icn3/phrase3/ti3.png" width="1" height="1">
        <img src="img/icn3/phrase4/chorba.png" width="1" height="1">
        <img src="img/icn3/phrase4/3asfour.png" width="1" height="1">
        <img src="img/icn3/phrase4/n7eb.png" width="1" height="1">
        <img src="img/icn3/phrase4/lsen.png" width="1" height="1">
        <img src="img/icn3/phrase5/7weyej.png" width="1" height="1">
        <img src="img/icn3/phrase5/tweet.png" width="1" height="1">
        <img src="img/icn3/phrase6/salla.png" width="1" height="1">
        <img src="img/icn3/phrase6/ftour.png" width="1" height="1">
        <img src="img/icn3/phrase6/youtube.png" width="1" height="1"> 
        <img src="img/icn3/phrase7/wa7cha.png" width="1" height="1">
        <img src="img/icn3/phrase7/like.png" width="1" height="1">
        <img src="img/icn3/phrase8/b.png" width="1" height="1">
        <img src="img/icn3/phrase8/lyoum.png" width="1" height="1">
        <img src="img/icn3/phrase8/slata.png" width="1" height="1">
        <img src="img/icn3/phrase8/yt.png" width="1" height="1">
        <img src="img/icn3/phrase8/link.png" width="1" height="1">
        <img src="img/icn3/phrase8/klit.png" width="1" height="1">
        <img src="img/icn3/phrase9/sou9a.png" width="1" height="1">
        <img src="img/icn3/phrase9/tayebet.png" width="1" height="1">
        <img src="img/icn3/phrase9/tajinn.png" width="1" height="1">
        <img src="img/icn3/phrase9/mail.png" width="1" height="1">
        <img src="img/icn3/phrase10/7oms.png" width="1" height="1">
        <img src="img/icn3/phrase10/100t.png" width="1" height="1">
        <img src="img/icn3/phrase10/atini.png" width="1" height="1">
        <img src="img/icn3/phrase10/gram.png" width="1" height="1">
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

    <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('js/jigLite.min.js') }}"></script>
    <script src="{{ asset('js/Draggable.min.js') }}"></script>
    <script src="{{ asset('js/jquery.timer.js') }}"></script>
    <script src="{{ asset('js/jquery.foggy.min.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script>
        var draggables=[];
        var currentGame = 1;
        var currentLevel = 0;
        var url = '';
        var pages = ['home','rank','reg']
        var activePage = 0;
        var cPl = 1;
        var game={};
    </script>
    <script src="{{ asset('js/main.js') }}?v=1.4"></script>

    @yield('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-49999304-13','auto');ga('send','pageview');
    </script>.
</body>
</html>
