@extends('layout')

@section('class', 'home')

@section('content')
    <div class="wrapper-all">

        <div class="game-ui">
            <div class="logo leftTop"> <img src="img/logo.png" alt=""></div>

            <div class="rightTop">
                <button class="sound-btn" data-toggle="tooltip" data-placement="bottom" title="Musique">
                    <i class="icon-music"></i>
                    <span class="active"></span>
                </button>
                <div class="menu-icn-wrapper">
                    <div class="menu-icn">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

            <div class="menu-wrapper">
                <div class="content">
                    <ul>
                        <li><a href="#" data-page="home"><span>Accueil</span> Revenir à l'accueil</a></li>
                        <li><a href="#" data-page="rank"><span>Classement</span> Découvrez le classement</a></li>
                        <li><a href="{{ asset('reglement.pdf') }}" target="_blank"><span>Réglement</span> Découvrez le réglement</a></li>
                    </ul>
                    <style>
                        .link-wrapper {
                            position: absolute;
                            z-index: 10000;
                            text-align: center;
                            bottom: 130px;
                            width: 100%;
                            background-color: rgb(88, 94, 101);
                        }
                        .link-wrapper .menu-link {
                            display: inline-block;
                            text-decoration: none;
                            font-weight: bold;
                            color: #fff;
                            font-size: 12px;
                        }
                    </style>
                    <div class="link-wrapper">
                        <a class="menu-link" target="_blank" href="https://www.facebook.com/ooredootn/photos/a.10150234863750268.461167.63762350267/10155793448975268/?type=1">Gagnants de la phase 1</a>
                        <a class="menu-link" target="_blank" href="https://www.facebook.com/ooredootn/photos/a.10150234863750268.461167.63762350267/10155810086015268/?type=1">Gagnants de la phase 2</a>
                    </div>

                </div>
            </div>


            <div class="help-wrapper">
                <div class="content">
                    <a class="how-to-btn-close" href="#"><i class="icon-cancel"></i></a>
                    <br>
                    <img src="img/help-screen.png" class="howto" alt="Comment jouer?">
                </div>
            </div>

            <div class="callto-wrapper">
                <div class="content">
                    <a class="callto-btn-close" href="#"><i class="icon-cancel"></i></a>
                    <h3>Niveau terminé</h3>
                    <p>
                        Améliorez votre score et augmentez <br>
                        votre chance de gagner en rejouant et en partageant le jeu Tchanchinet avec vos amis.
                    </p>

                    <a href="#" class="share-fb"><img src="img/share-btn.png" alt=""></a>

                    <h3>Découvrez nos offres <br> et promos</h3>

                    <a href="http://ooredoo.tn/particuliers/promo-netkallem" target="_blank"><img src="img/nte-btn.png" alt=""></a>
                    <a href="http://ooredoo.tn/particuliers/familia" target="_blank"><img src="img/fam-btn.png" alt=""></a>
                </div>
            </div>

            <div class="ranking-wrapper">
                <div class="content">
                    <div class="close-btn" onclick="closeRank()"><i class="icon-cancel"></i></div>
                    <h2>Classement</h2>

                    <ul class="user-ranking">
                        @forelse($classement as $index => $player)
                            <li>
                                <div class="rank">{{ $index + 1 }}</div>
                                <div class="picture"><img src="https://graph.facebook.com/{{ $player->facebook_id }}/picture?width=200&height=200" alt="{{ $player->name }}"></div>
                                <div class="info">
                                    <h4 class="name">{{ $player->name }}</h4>
                                    <h5 class="score">{{ $player->score }} Point(s)</h5>
                                </div>
                            </li>
                        @empty
                            <li>
                                <div class="info">
                                    <h4 class="name">Aucun joueur actuellement.</h4>
                                </div>
                            </li>
                        @endforelse
                    </ul>

                    <div class="my-info hide clearfix">
                        <h3 class="title">Mon score</h3>
                        <div class="rank">2</div>
                        <div class="picture"><img src="img/profile.jpg" alt="User Name"></div>
                        <div class="info">
                            <h4 class="name">Foulen Ben Foulen</h4>
                            <h5 class="score"><span></span> Point</h5>
                        </div>
                    </div>
                    <button class="share-fb btn"><i class="icon-facebook"></i> Partagez</button>
                    <button class="repeat btn hide"><i class="icon-home"></i> Niveaux</button>

                </div>
            </div>
            <div class="ranking-wrapper-back"></div>

            <div class="logo-inner">
                <img src="img/left-logo-tcha.png" alt="">
            </div>

            <div class="leftBottom">
                <a href="#" class="fb-share"><i></i></a>
                <a href="#" class="tw-share"></a>
            </div>

        </div>

        <div id="game-wrapper">

            <div class="page-wrapper">

                <div class="floating">
                    <img src="img/cloud-top.png" class="top-cloud" alt="">
                    <img src="img/cloud-right.png" class="right-cloud" alt="">
                    <img src="img/cloud-left.png" class="left-cloud" alt="">
                </div>


                <!--
                    Home Page
                ===================== -->
                <div class="page home">

                    <div class="logo"><img src="img/logo-middle.png" alt="Tchanchinet"></div>
                    <div class="forest"><img src="img/forest.png" alt=""></div>
                    <div class="icones"><img src="img/icons-home.png" alt=""></div>
                    <div class="keyboard"><img src="img/keyboard.png" alt=""></div>

                    <button class="login-btn action-btn">
                        <span class="pull-left">participez
                        <span class="sub">cliquez ici</span></span>
                        <i class="icon-right"></i>
                    </button>

                    <p>Devinez les mots et les phrases en déplaçant votre souris pour les compléter avec les icônes proposées.</p>

                </div>


                <!--
                    Form Page
                ===================== -->
                <div class="page form hide">
                    <form id="signup-form" action="{{ action('AppController@signup') }}" method="post" accept-charset="utf-8" class="container">
                        {!! csrf_field() !!}
                        <img src="img/form-title.png" class="title" alt="">

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Nom et prénom *" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <input type="tel" name="phone" class="form-control" placeholder="Téléphone" maxlength="8">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email *" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <input type="test" class="form-control" name="cin" placeholder="CIN *" required maxlength="8">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p class="info"><small>* Champ obligatoire</small><br> En cliquant sur "Validez", vous acceptez le <a href="reglement.pdf" target="_blank">règlement du jeu</a></p>
                                <button type="submit" class="action-btn submit-btn">
                                    <span class="pull-left">validez
                                    <span class="sub">cliquez ici</span></span>
                                    <i class="icon-right"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="forest"><img src="img/forest.png" alt=""></div>
                    <div class="keyboard"><img src="img/keyboard.png" alt=""></div>

                </div>


                <!--
                    Levels Page
                ===================== -->
                <div class="page stages hide">
                    <div class="content">
                        <p class="des text-center">Vous avez la possibilité de choisir <br> le niveau dès qu’il est accessible à la date indiquée.</p>
                        <div class="row">
                            <div class="col-xs-4">
                                <button class="startGame btn-stage active" data-index="1">
                                    <span class="title">Niveau 1</span>
                                    <i> Jouez </i>
                                </button>
                                <span class="score"></span>
                            </div>
                            <div class="col-xs-4">
                                <button class="startGame btn-stage disabled" disabled data-index="2" data-locked="{{ new DateTime() > new DateTime('2015-07-08') ? 'false' : 'true' }}">
                                    <span class="title">Niveau 2</span>
                                    <i class="icon-lock"></i>
                                    <span class="date">08/07/15</span>
                                </button>
                                <span class="score"></span>
                            </div>
                            <div class="col-xs-4">
                                <button class="startGame btn-stage disabled" disabled data-index="3" data-locked="{{ new DateTime() > new DateTime('2015-07-13') ? 'false' : 'true' }}">
                                    <span class="title">Niveau 3</span>
                                    <i class="icon-lock"></i>
                                    <span class="date">13/07/15</span>
                                </button>
                                <span class="score"></span>
                            </div>
                        </div>

                    </div>
                    <div class="forest"><img src="img/forest.png" alt=""></div>
                    <div class="keyboard"><img src="img/keyboard.png" alt=""></div>
                </div>

                <!--
                    Game Page
                ===================== -->
                <div class="page game hide">
                    <div id="drag-land">

                        <div class="next-alert">
                            <div class="content">
                                <h3>Bravo! la réponse est : </h3>
                                <div class="rep"></div>
                                <p>On passe à la question suivante</p>
                            </div>
                        </div>

                        <div id="time"><i class="icon-watch"></i> <span>00:00:00</span></div>
                        <div class="drops"></div>
                        <div class="drags"></div>
                    </div>
                    <button class="action-btn" id="verify">
                        <span class="pull-left">validez
                        <span class="sub">cliquez ici</span></span>
                        <i class="icon-right"></i>
                    </button>
                </div>

            </div>


            <!--
                floating elements
            ===================== -->

            <div class="info-widget">
                <div class="picture"><img src="img/profile.jpg" alt="User Name"></div>
                <div class="info">
                    <h4></h4>
                    <h5>Score : <span>0</span></h5>
                    <p><i class="icon-lock-open"></i> <i class="icon-lock"></i> <i class="icon-lock"></i></p>
                </div>
            </div>

            <div class="how-to"><a href="#" class="how-to-btn"><img src="img/help-label.png" alt=""></a></div>

            <img src="img/bg-elements.png" class="elements-bg" />

        </div>
    </div>
@stop

@section('title', 'لعب معانا التشنشي net و شيخ روحك بالكادوات.')
@section('description', 'تفهملها في النات و في سبرك الانترنات، العب معانا و طلع التشنشي net و شيخ روحك بالكادوات.')



